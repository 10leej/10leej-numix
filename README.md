An icon theme I created with [Oomox](https://github.com/themix-project/oomox) based on the [Numix](https://github.com/numixproject/numix-icon-theme) icon theme.
